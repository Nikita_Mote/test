"use strict";

module.exports = {
  checkLogIn: function (req, res, next) {
    if (req.isAuthenticated()) {
      return next();
    }

    res.send("Authentication failed!!");
  },
};
