var createError = require("http-errors");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
const passport = require("passport");
const LocalStrategy = require("passport-local").Strategy;

const CryptoJS = require("crypto-js");
const bookshelf = require("./db-config");

var indexRouter = require("./routes/index");
var usersRouter = require("./routes/users");

var app = express();

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "jade");

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));
app.use(require("body-parser").urlencoded({ extended: true }));
app.use(
  require("express-session")({
    secret: "keyboard cat",
    resave: true,
    saveUninitialized: true,
  })
);
app.use(passport.initialize());
app.use(passport.session());
app.use("/main", indexRouter);
app.use("/", usersRouter);

app.use(function (req, res, next) {
  res.locals.user = req.user;
  next();
});

passport.use(
  new LocalStrategy(async function (username, password, done) {
    var UserTable = bookshelf.Model.extend({
      tableName: "users",
    });
    let activeUser = null;
    username = username.toLowerCase();

    try {
      activeUser = await UserTable.where({
        username: username,
      })
        .fetch()
        .then(function (data) {
          if (data == null) {
            return data;
          } else {
            return data.toJSON();
          }
        });
    } catch (err) {
      return done(err, null);
    }
    let user = activeUser;

    if (!user) {
      let err = new Error("Invalid username");
      return done(err, null);
    }
    var hashPassword = CryptoJS.SHA512(password);
    // var doubleHash = CryptoJS.SHA512(hashPassword.toString());
    let isMatch = false;
    if (hashPassword.toString() == user.password) {
      isMatch = true;
    }

    if (!isMatch) {
      let err = new Error("Invalid password");
      return done(err, null);
    }
    return done(null, user);
  })
);

passport.serializeUser(function (user, done) {
  return done(null, user);
});

passport.deserializeUser(function (user, done) {
  return done(null, user);
});

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

module.exports = app;
