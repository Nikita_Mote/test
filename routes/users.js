var express = require("express");
var router = express.Router();
const passport = require("passport");
const helper = require("../lib/helper");
const CryptoJS = require("crypto-js");
const bookshelf = require("../db-config");

var UserTable = bookshelf.Model.extend({
  tableName: "users",
});
/* GET users listing. */
router.get("/", function (req, res, next) {
  res.send("respond with a resource");
});

// To save user login details
router.post("/login", async function (req, res, next) {
  console.log(req.body);
  await passport.authenticate("local", function (err, user, info) {
    if (err) {
      res.send({
        error: true,
        errorMessage:
          "The username and password combination you tried to log in with is incorrect. Please try again.",
      });
    }

    if (!user) {
      res.send({
        error: true,
        errorMessage:
          "The username and password combination you tried to log in with is incorrect. Please try again.",
      });
    }

    req.logIn(user, function (err) {
      if (err) {
        res.send({
          error: true,
          errorMessage:
            "The username and password combination you tried to log in with is incorrect. Please try again.",
        });
      }
      res.send({
        error: false,
        successMessage: "You are loggedin Successfully",
      });
    });
  })(req, res, next);
});

//GET of  ports List
router.post("/users", async (req, res, next) => {
  console.log("in post");
  try {
    await UserTable.where({
      employeeid: req.body.empid,
    })
      .fetchAll()
      .then(async function (user) {
        if (user.toJSON().length == 0) {
          var hashPassword = CryptoJS.SHA512(req.body.password).toString();
          new UserTable({
            firstname: req.body.firstname,
            lastname: req.body.lastname,
            username: req.body.username,
            password: hashPassword,
            organization: req.body.organization,
            employeeid: req.body.empid,
            created_when: new Date(),
            created_by: 1,
          }).save(null, {
            method: "insert",
          });
          res.send("Registration done");
        } else {
          res.send("registartion already done with same employee id");
        }
      });
  } catch (err) {
    console.log(err);
    err.message = "Error while registration";
    return next(err);
  }
});

router.get("/users", helper.checkLogIn, async (req, res, next) => {
  let users = null;
  try {
    users = await UserTable.fetchAll().then(function (data) {
      return data.toJSON();
    });
  } catch (err) {
    err.message = "Error while retrieving user list:";
    return next(err);
  }
  res.json(users);
});
module.exports = router;
